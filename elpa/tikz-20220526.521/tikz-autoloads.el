;;; tikz-autoloads.el --- automatically extracted autoloads  -*- lexical-binding: t -*-
;;
;;; Code:

(add-to-list 'load-path (directory-file-name
                         (or (file-name-directory #$) (car load-path))))


;;;### (autoloads nil "tikz" "tikz.el" (0 0 0 0))
;;; Generated autoloads from tikz.el

(autoload 'tikz-mode "tikz" "\
Drawing interface for TikZ pictures.

This is a minor mode.  If called interactively, toggle the `TikZ
mode' mode.  If the prefix argument is positive, enable the mode,
and if it is zero or negative, disable the mode.

If called from Lisp, toggle the mode if ARG is `toggle'.  Enable
the mode if ARG is nil, omitted, or is a positive number.
Disable the mode if ARG is a negative number.

To check whether the minor mode is enabled in the current buffer,
evaluate `tikz-mode'.

The mode's hook is called both when the mode is enabled and when
it is disabled.

Compile your (short) one page TeX file with TikZ pictures.

Do not modify your preamble. If you change it, turn off this mode
and turn on it again.

Similar to the program QtikZ.

\(fn &optional ARG)" t nil)

(register-definition-prefixes "tikz" '("tikz-"))

;;;***

;;;### (autoloads nil nil ("tikz-pkg.el") (0 0 0 0))

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; tikz-autoloads.el ends here
