(define-package "tikz" "20220526.521" "A minor mode to edit TikZ pictures"
  '((emacs "24.1"))
  :commit "4b205afc5c88f050639135d1d57f1276db323842" :authors
  '(("Emilio Torres-Manzanera" . "torres@uniovi.es"))
  :maintainer
  '("Emilio Torres-Manzanera" . "torres@uniovi.es")
  :keywords
  '("tex")
  :url "https://github.com/emiliotorres/tikz")
;; Local Variables:
;; no-byte-compile: t
;; End:
